import tornado.ioloop
import tornado.web
import tornado.template
import os, os.path, time


class PageHandler(tornado.web.RequestHandler):
    def build(self, template, template_args):
        base = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
        loader = tornado.template.Loader(os.path.join(base, 'template'))
        self.write(loader.load("header.html").generate())
        self.write(loader.load(template + ".html").generate(**template_args))
        self.write(loader.load("footer.html").generate())

class IndexHandler(PageHandler):
    def get(self):
        self.build('index', {
            "feeding_graph": "Position of the feeding dish in degrees.",
            "temperature_graph": "Temperature of the aquarium in degrees Celsius.",
            "water_level_graph": "Capacitance of the water level measuring capacitor in pF.",
            "brightness_graph":  "The voltage in V of the solar cell."
        })

class FeedingHandler(PageHandler):
    def get(self):
        self.build('feeding', {

        })

class TemperatureHandler(PageHandler):
    def get(self):
        self.build('temperature', {

        })

class BrightnessHandler(PageHandler):
    def get(self):
        self.build('brightness', {

        })

class WaterLevelHandler(PageHandler):
    def get(self):
        self.build('water-level', {

        })

class BudgetHandler(PageHandler):
    def get(self):
        self.build('budget', {

        })

class ReceiveHandler(tornado.web.RequestHandler):
    def post(self):
        f = self.get_argument('file', False)
        if not f:
            self.write("file")
            return
        f = f.replace('/','_').replace('\\','_')

        data = self.get_argument('data', False)
        if not data:
            self.write("data")
            return
        base = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))

        folder = os.path.join(base, "data", time.strftime("%Y-%m-%d"))
        if not os.path.exists(folder):
            os.makedirs(folder)
        target = os.path.join(folder, f)

        with open(target, "a") as f:
            f.write(data + "\n")
        self.write("OK")

def make_app():
    base = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
    data_path   = os.path.join(base, 'data')
    static_path = os.path.join(base, 'public')
    return tornado.web.Application([
        (r"/", IndexHandler),
        (r"/feeding", FeedingHandler),
        (r"/temperature", TemperatureHandler),
        (r"/brightness", BrightnessHandler),
        (r"/water-level", WaterLevelHandler),
        (r"/budget", BudgetHandler),
        (r"/receive", ReceiveHandler),
        (r"/data/(.*)", tornado.web.StaticFileHandler, {'path': data_path}),
        (r"/(.*)", tornado.web.StaticFileHandler, {'path': static_path})
    ])

if __name__ == "__main__":
    app = make_app()
    app.listen(8888)
    tornado.ioloop.IOLoop.current().start()
