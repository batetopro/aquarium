import serial
import time
import os
import httplib2, urllib.parse

port = 'COM3'
rate = 9600
sendLines = 200
lines = 0

def append_data(path, data):
    base = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
    path = os.path.join( base, 'recorded', path + '.csv')
    with open(path, 'a') as file:
        file.write("%d,%s\n" % (int(time.time()), data))

def send_data():
    base = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
    path = os.path.join( base, 'recorded')
    for f in os.listdir(path):
        f = os.path.join(path, f)
        if not os.path.isfile(f):
            continue

        with open(f, 'r') as datafile:
            data=datafile.read()
        os.remove(f)

        values = {
            'file' : f.split('\\')[-1],
            'data' : data
        }

        data = urllib.parse.urlencode(values)
        headers = {'Content-type': 'application/x-www-form-urlencoded'}
        h = httplib2.Http()

        url = "http://nashiterecepti.com:8888/receive"
        #url = "http://localhost:8888/receive"
        resp, content = h.request(url, "POST", headers=headers, body=data)

usb = serial.Serial(port, rate, timeout=.1)
while True:
    data = usb.readline().strip()
    if data:
        data  = data.decode('utf-8')
        parts = data.split('=')
        append_data(parts[0], parts[1])
        lines += 1
        if lines > sendLines:
            send_data()
            lines = 0
