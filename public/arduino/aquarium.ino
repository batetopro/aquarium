#include <Servo.h>

#define T_PIN   A0
#define V_PIN   A1
#define C1_PIN  A2
#define C2_PIN  A3
#define M_PIN   9
#define DT 10000
#define NUM_SAMPLES 10
Servo myServo;  // create a servo object

float VPOW = 4.7;
float T0 = 20.0;

int M_A = 0;
int M_delta = 20;
long M_time = 0;
long M_step = 300000;

//Capacitance between IN_PIN and Ground
//Stray capacitance is always present. Extra capacitance can be added to
//allow higher capacitance to be measured.
const float IN_STRAY_CAP_TO_GND = 24.48; //initially this was 30.00
const float IN_EXTRA_CAP_TO_GND = 0.0;
const float IN_CAP_TO_GND  = IN_STRAY_CAP_TO_GND + IN_EXTRA_CAP_TO_GND;
const int MAX_ADC_VALUE = 1023;


// Temperature
float readT(){
  float V = (analogRead(T_PIN) / 1024.0) * 5.0;
  float T = (V - .5) * 100;
  return T;
} 
void initT(){
  T0 = readT();
}
void stepT(){
  Serial.print("T=");
  Serial.println(readT());
}

// Voltage
void stepV(){
  int sum = 0;                    // sum of samples taken
  unsigned char sample_count = 0; // current sample number
  float voltage = 0.0;            // calculated voltage
  while (sample_count < NUM_SAMPLES) {
      sum += analogRead(V_PIN);
      sample_count++;
      delay(10);
  }
  // calculate the voltage
  // use 5.0 for a 5.0V ADC reference voltage
  // 5.015V is the calibrated reference voltage
  voltage = ((float)sum / (float)NUM_SAMPLES * 5.015) / 1024.0;
  // send voltage for display on Serial Monitor
  // voltage multiplied by 11 when using voltage divider that
  // divides by 11. 11.132 is the calibrated voltage divide
  // value
  Serial.print("V=");
  Serial.println(voltage * 11.132);
  sample_count = 0;
  sum = 0;
}

// Capacity
void initC(){
  pinMode(C1_PIN, OUTPUT);
  pinMode(C2_PIN, OUTPUT);
}

void stepC(){
  pinMode(C1_PIN, INPUT);
  digitalWrite(C2_PIN, HIGH);
  int val = analogRead(C1_PIN);
  
  //Clear everything for next measurement
  digitalWrite(C2_PIN, LOW);
  pinMode(C1_PIN, OUTPUT);

  //Calculate and print result

  float capacitance = (float)val * IN_CAP_TO_GND / (float)(MAX_ADC_VALUE - val);

  Serial.print("C=");
  Serial.println(capacitance, 3);
  
  while (millis() % 500 != 0)
    ; 
}


// Driver
void initM(){
  myServo.attach(M_PIN);
  myServo.write(M_A);
}

void M(){
  M_A += M_delta;
  if (M_A  > 175){
    M_A  = 0; 
  }
  myServo.write(M_A);
  Serial.print("M=");
  Serial.println(M_A);
}

void setup() {
  Serial.begin(9600);
  initT();
  initC();
  initM();
}

void loop() {
  stepT();
  stepV();
  stepC();
  if (M_time >=  M_step){
    M();
    M_time = 0;
  }
  M_time += DT;
  delay(DT);
}
