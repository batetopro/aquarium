var load_data = function(name, callback){

    var dt = $("#datepicker").val();
    $.get('/data/'+dt+"/"+name+'.csv', function(data){
        var lines = data.split('\n');
        var plot  = [];
        for(var i = 0;i < lines.length;i++){
            var parts = lines[i].split(',');
            if(parts.length != 2){continue;}
            plot.push([parts[0], parts[1]]);
        }
       callback(plot);
    }).fail(function() {
       callback([]);
    });
}

var plot = function(name, container, label, unit){
    load_data(name, function(plot){
        if(plot.length == 0){
            $(container).html("<strong>No data for this period</strong>");
            return;
        }
        $.each(plot.reverse(), function(i,row){
            plot[i][0] *= 1000;
        });
		var options = {
			canvas: true,
			xaxes: [ { mode: "time" } ],
			yaxes: [ { min: 0 }, {
				position: "right",
				alignTicksWithAxis: 1,
				tickFormatter: function(value, axis) {
					return value.toFixed(axis.tickDecimals) + "€";
				}
			} ],
			legend: { position: "sw" },
			series: {
				lines: {
					show: true
				},
				points: {
					show: true
				}
			},
			grid: {
				hoverable: true
			}
		}

		var data = [
			{ data: plot, label: label }
		];

        $.plot($(container), data, options);
    });

    $(container).bind("plothover", function (event, pos, item) {
        if (item) {
            var x = item.datapoint[0].toFixed(2),
                y = item.datapoint[1].toFixed(2);
            console.log(x);
            var date = new Date(parseInt(x)).format("MM-dd hh:mm");
            $("#tooltip").html(date + " = " + y + " " + unit)
                .css({top: item.pageY+5, left: item.pageX+5})
                .fadeIn(200);
        } else {
            $("#tooltip").hide();
        }
    });
}

var build_table = function(name, container){
    load_data(name, function(data){
        if(data.length == 0){
            $(container).html("<strong>No data for this period</strong>");
            return;
        }
        var cont = $('<ul class="list-group">');
        $.each(data.reverse(), function(i,row){
            var date = new Date(row[0]*1000);
            var li = $('<li class="list-group-item">');
            li.text(date.format("MM-dd hh:mm"));
            li.append('<span class="badge badge-success pull-right">'+row[1]+'</span>');
            cont.append(li);
        });
        $(container).html(cont);
    });
}


Date.prototype.format = function(format) //author: meizz
{
  var o = {
    "M+" : this.getMonth()+1, //month
    "d+" : this.getDate(),    //day
    "h+" : this.getHours(),   //hour
    "m+" : this.getMinutes(), //minute
    "s+" : this.getSeconds(), //second
    "q+" : Math.floor((this.getMonth()+3)/3),  //quarter
    "S" : this.getMilliseconds() //millisecond
  }

  if(/(y+)/.test(format)) format=format.replace(RegExp.$1,
    (this.getFullYear()+"").substr(4 - RegExp.$1.length));
  for(var k in o)if(new RegExp("("+ k +")").test(format))
    format = format.replace(RegExp.$1,
      RegExp.$1.length==1 ? o[k] :
        ("00"+ o[k]).substr((""+ o[k]).length));
  return format;
}

$(function(){
    $( "#datepicker" ).datepicker({
        maxDate: 0
    });

    $( "#datepicker" ).val(
        $.datepicker.formatDate("yy-mm-dd", new Date())
    );

    build_table('M', "#plot-feeding");
    plot('T', "#plot-temperature", "Temperature in degrees C", "C");
    plot('C', "#plot-level", "Capacitance in pF", "pF");
    plot('V', "#plot-brightness", "Voltage of the solar cell in V", "Vs");

    $("<div id='tooltip'></div>").css({
			position: "absolute",
			display: "none",
			border: "1px solid #fdd",
			padding: "2px",
			"background-color": "#fee",
			opacity: 0.80
		}).appendTo("body");
});